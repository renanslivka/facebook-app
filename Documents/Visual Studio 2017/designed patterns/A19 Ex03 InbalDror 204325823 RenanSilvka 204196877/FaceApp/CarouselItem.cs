﻿namespace FaceApp
{
    public class CarouselItem
    {
        public string m_ImageUrl;
        public string m_Title;
        public string m_Description;
  
        public CarouselItem(string i_Title, string i_Description, string i_ImageUrl)
        {
            this.m_Title = i_Title;
            this.m_Description = i_Description;
            this.m_ImageUrl = i_ImageUrl;
        }
    }
}
