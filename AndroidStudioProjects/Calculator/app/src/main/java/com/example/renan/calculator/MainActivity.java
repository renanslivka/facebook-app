package com.example.renan.calculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

    TextView resTv;
    int lastNum;
    String last_action;
    boolean actionPress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resTv  = findViewById(R.id.enter_line);

        TextView textview = findViewById(R.id.enter_line);
        Button button0 = findViewById(R.id.num0);
        Button button1 = findViewById(R.id.num1);
        Button button2 = findViewById(R.id.num2);
        Button button3 = findViewById(R.id.num3);
        Button button4 = findViewById(R.id.num4);
        Button button5 = findViewById(R.id.num5);
        Button button6 = findViewById(R.id.num6);
        Button button7 = findViewById(R.id.num7);
        Button button8 = findViewById(R.id.num8);
        Button button9 = findViewById(R.id.num9);
        Button answerBtn = findViewById(R.id.answer);
        Button buttonMul = findViewById(R.id.mul);
        Button buttonAdd = findViewById(R.id.add);
        Button buttonSub = findViewById(R.id.sub);
        Button buttonDiv = findViewById(R.id.div);
        Button buttonDelete = findViewById(R.id.C);

        NumbersButtonsListener numbersButtonsListener = new NumbersButtonsListener();

        button0.setOnClickListener(numbersButtonsListener);
        button1.setOnClickListener(numbersButtonsListener);
        button2.setOnClickListener(numbersButtonsListener);
        button3.setOnClickListener(numbersButtonsListener);
        button4.setOnClickListener(numbersButtonsListener);
        button5.setOnClickListener(numbersButtonsListener);
        button6.setOnClickListener(numbersButtonsListener);
        button7.setOnClickListener(numbersButtonsListener);
        button8.setOnClickListener(numbersButtonsListener);
        button9.setOnClickListener(numbersButtonsListener);

        ActionBtnListener actionBtnListener = new ActionBtnListener();

        answerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evaluate();
            }
        });
        buttonMul.setOnClickListener(actionBtnListener);
        buttonAdd.setOnClickListener(actionBtnListener);
        buttonSub.setOnClickListener(actionBtnListener);
        buttonDiv.setOnClickListener(actionBtnListener);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resTv.setText("");
            }
        });

    }
    private void evaluate(){
        if (last_action == null)return;
        String numStr = resTv.getText().toString();
        int newNum = Integer.parseInt(numStr);

        switch (last_action){
            case "+":
                resTv.setText(lastNum+newNum+"");
                break;
            case"-":
                resTv.setText(lastNum-newNum+"");
                break;
            case"X":
                resTv.setText(lastNum*newNum+"");
                break;
            case"/":
                resTv.setText((double)lastNum/newNum+"");
                break;
        }
        actionPress = true;
        last_action = null;
    }
    private class NumbersButtonsListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v) {
            String numStr = ((Button)v).getText().toString();
            if (actionPress)
            {
                resTv.setText(((Button)v).getText().toString());
                resTv.setText(numStr);
                actionPress = false;
            }
            else {
                resTv.setText(resTv.getText() + numStr);
            }
        }

    }

    private class ActionBtnListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v) {
            if (last_action != null) evaluate();
            String numStr = resTv.getText().toString();
            lastNum = Integer.parseInt(numStr);
            last_action = ((Button)v).getText().toString();
            actionPress = true;
        }
    }
}
