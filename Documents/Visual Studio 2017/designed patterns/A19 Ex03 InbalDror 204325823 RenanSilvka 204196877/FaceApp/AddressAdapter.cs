﻿using FacebookWrapper.ObjectModel;

namespace FaceApp
{
    public class AddressAdapter
    {
        private readonly string r_Lat;
        private readonly string r_Lon;
       
        public string m_AddresString { get; set; }

        public AddressAdapter(Location location)
        {
            this.r_Lat = location.Latitude.ToString();
            this.r_Lon = location.Longitude.ToString();
            m_AddresString = string.Format("https://www.google.co.il/maps/place/{0},+{1}/@{0},{1},15z", r_Lat, r_Lon);
        }
    }
}
