﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex03.GarageLogic
{
    public class FuelTruck : Vehicle
    {        
        //private readonly float m_CapacityOfCargo = 115;
        //private float m_CurrAmountFuelLiters;
        public bool IsTheTrunkCooled = true;

        public FuelTruck(
            bool IsTheTrunkCooled,
            string ModelName,
            string LicenseNumber,
            int numbersOfWheels,
            string m_ManufacturerName,
            float m_CurrentAirPressure,
            float m_MaxAirPressure, float CurrAmountTank, ConditionInTheGarage i_ConditionInTheGarage, string m_OwnersName, string i_PhoneName)
            : base(ModelName, LicenseNumber, 12, m_ManufacturerName, m_CurrentAirPressure,
                  28, FuelTypes.Soler,115, CurrAmountTank, ConditionInTheGarage.InRepair, m_OwnersName, i_PhoneName)
        {}

        public override void FillTank(float AmountFuelLitersToAdd, FuelTypes i_FuelType)
        {
            if (i_FuelType != this.m_FuelType)
            {
                return;
            }
            float curr_amount = this.get_m_CurrAmountTank();
            if ((this.m_CapacityOfCargo - curr_amount) > AmountFuelLitersToAdd)
            {
                set_m_CurrAmountTank(curr_amount + AmountFuelLitersToAdd);
            }

        }
    }

}

