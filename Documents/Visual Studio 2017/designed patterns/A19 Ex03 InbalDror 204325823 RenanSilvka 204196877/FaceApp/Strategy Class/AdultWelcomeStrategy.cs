﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceApp.Strategy_Class
{
    public class AdultWelcomeStrategy : IWelcomeStrategy
    {
        public void SayHi(int age)
        {
            MessageBox.Show("Hi you are Adult ,your age is " + age.ToString());
        }
    }
}
