﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceApp.Strategy_Class
{
    public interface IWelcomeStrategy
    {        
        void SayHi(int age);
    }
}
