﻿using FacebookWrapper.ObjectModel;

namespace FaceApp
{
    public class CarouselItemLocation
    {
        public string m_Lat;
        public string m_Lon;
        public Page m_Place;

        public CarouselItemLocation(Page i_Place, string i_Lat, string i_Lon)
        {
            this.m_Place = i_Place;
            this.m_Lat = i_Lat;
            this.m_Lon = i_Lon;
        }
    }
}
