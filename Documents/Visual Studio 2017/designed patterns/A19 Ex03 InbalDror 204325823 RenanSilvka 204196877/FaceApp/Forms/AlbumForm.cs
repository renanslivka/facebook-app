﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Forms;
using FacebookWrapper.ObjectModel;

namespace FaceApp
{
    public partial class AlbumForm : Form
    {
        private const int k_NumOfPicToShow = 4;
        private PictureBox[] m_PicToShow = new PictureBox[k_NumOfPicToShow];
        private Photo[] m_TaggedPic = new Photo[k_NumOfPicToShow];

        private UserFacebook m_UserFacebook;
        private WindowSettings m_WindowSettings;
        private Carousel m_Carousel;
        private List<Album> m_Items;
        public IBridge Bridge;

        public AlbumForm()
        {
            InitializeComponent();
            this.m_UserFacebook = UserFacebook.getInstance();
            this.m_WindowSettings = WindowSettings.getInstance();
            this.m_Items = new List<Album>();
            m_WindowSettings.SetAlbumPage(this);
            addFavoritePictureBox();
        }

        private void addFavoritePictureBox()
        {
            m_PicToShow[0] = pictureBoxFavPic1;
            m_PicToShow[1] = pictureBoxFavPic2;
            m_PicToShow[2] = pictureBoxFavPic3;
            m_PicToShow[3] = pictureBoxFavPic4;
        }

        private void AlbumPages_Load(object sender, EventArgs e)
        {
            if (m_UserFacebook.LoggedInUser.Gender.ToString() == "male")
            {
                Bridge = new ManColor();
                Bridge.ChangeBackColor(this);
            }
            else
            {
                Bridge = new WomanColor();
                Bridge.ChangeBackColor(this);
            }

            new Thread(fetchAlbom).Start();
        }

        private List<CarouselItem> prepareItems(List<Album> list)
        {
            List<CarouselItem> listOfItems = new List<CarouselItem>();
            foreach (Album item in list)
            {
                CarouselItem c_item = new CarouselItem(item.Name, item.Description, item.PictureSmallURL);
                listOfItems.Add(c_item);
            }

            return listOfItems;
        }
        
        private void fetchAlbom()
        {
            foreach (Album album in this.m_UserFacebook.LoggedInUser.Albums)
            {
                if (album.Name != null)
                {
                    this.m_Items.Add(album);
                }
                else if (album.Link != null)
                {
                    this.m_Items.Add(album);
                }
            }

            if (this.m_UserFacebook.LoggedInUser.Albums.Count == 0)
            {
                MessageBox.Show("No Album to retrieve :(");
            }
            else
            {
                m_Carousel = new Carousel(this.pictureBox, this.labelName, this.prepareItems(this.m_Items));
            }
        }

        private void buttonForward_Click(object sender, EventArgs e)
        {
            m_Carousel.DisplayForward();
        }
        
        private void buttonBack_Click(object sender, EventArgs e)
        {
            m_Carousel.DisplayPrevious();
        }

        private void buttonBackToProfilePage_Click(object sender, EventArgs e)
        {
            this.Hide();
            ProfilePageForm window = new ProfilePageForm();
            window.ShowDialog();
            this.Close();
        }

        private void HowManyTagesBar_Scroll(object sender, EventArgs e)
        {
            labelNumOfLikes.Text = HowManyTagesBar.Value.ToString();
        }

        private void buttonFavoritePic_Click(object sender, EventArgs e)
        {
            if (m_Carousel.m_LabelName != null)
            {
                m_TaggedPic = showFavoritePhotos(HowManyTagesBar.Value);
            }
            else
            {
                MessageBox.Show("You must choose Album first !");
            }

            if (m_TaggedPic[0] != null)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (m_TaggedPic[i] != null)
                    {
                        m_PicToShow[i].ImageLocation = m_TaggedPic[i].PictureAlbumURL;
                    }
                    else
                    {
                        m_PicToShow[i].Image = null;
                    }
                }
            }
            else
            {
                MessageBox.Show("There are no photos!");
            }
        }

        public Photo[] showFavoritePhotos(int i_MinTag)
        {
            int count = 0;
            Photo[] favoritePhoto = new Photo[k_NumOfPicToShow];
            HowManyTagesBar.Enabled = false;
            foreach (Album FbAlbum in m_UserFacebook.LoggedInUser.Albums)
            {
                if (FbAlbum.Name.Equals(m_Carousel.m_LabelName.Text))
                {
                    foreach (Photo FbPhoto in FbAlbum.Photos)
                    {
                        if (FbPhoto.Tags != null)
                        {
                            if (FbPhoto.Tags.Count > i_MinTag)
                            {
                                favoritePhoto[count] = FbPhoto;
                                count++;
                            }
                        }

                        if (count == k_NumOfPicToShow)
                        {
                            break;
                        }
                    }

                    if (count == k_NumOfPicToShow)
                    {
                        break;
                    }
                }
            }

            HowManyTagesBar.Enabled = true;
            return favoritePhoto;
        }
    }
}
