package com.example.renan.shoppinglist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class EditPhoneNumberActivity extends Activity {

    private String phoneNumber;
    private EditText phoneNumberEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_phone_number);

        phoneNumber = getIntent().getStringExtra("phone_number");
        phoneNumberEt = findViewById(R.id.phone_number_EditPhone);
        phoneNumberEt.setText(phoneNumber);

        Button cancel_Btn = findViewById(R.id.cancel_btn_e);
        cancel_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditPhoneNumberActivity.this,SettingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        Button ok_Btn = findViewById(R.id.ok_btn_e);
        ok_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditPhoneNumberActivity.this,MainActivity.class);
                intent.putExtra("updated_phone_number", phoneNumberEt.getText().toString());
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }
}
